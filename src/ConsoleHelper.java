import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * artes on 9/08/16 tingaev@gmail.com
 **/
class ConsoleHelper {
    private static volatile ConsoleHelper instance;

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private int anInt;
    private String aString;
    private double aDouble;

    private ConsoleHelper() {
    }

    static ConsoleHelper getInstance() {
        if (instance == null)
            synchronized (ConsoleHelper.class) {
                if (instance == null)
                    instance = new ConsoleHelper();
            }
        return instance;
    }

    int getInt() {
        boolean isValid = false;
        while (!isValid) {
            try {
                anInt = Integer.parseInt(reader.readLine().trim());
                isValid = true;
            }
            catch (Exception e) {
                System.out.println("Ошибка. Значение не является целым числом.");
            }
        }
        return anInt;
    }

    public double getDouble() {
        boolean isValid = false;
        while (!isValid) {
            try {
                aDouble = Double.parseDouble(reader.readLine().trim());
                isValid = true;
            } catch (IOException e) {
                System.out.println("Ошибка. Значение не является числом с плавающей точкой.");
            }

        }
        return aDouble;
    }
    String getString() {
        boolean isValid = false;
        while (!isValid) {
            try {
                aString = reader.readLine().trim();
            } catch (IOException e) {
                System.out.println("Ошибка. Строка пустая.");
            }
            if (!aString.isEmpty()) isValid = true;
        }
        return aString;
    }
}
