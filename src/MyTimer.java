
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * artes on 8/08/16. tingaev@gmail.com
 **/
class MyTimer implements Runnable {
    private int duration;
    private String unit;

    private enum timeUnit {SECONDS, MINUTES, HOURS, DAYS}

    private Map<String, String> mapOfDeclension = new HashMap<>(4);
    private Map<String, Enum> mapOfEnums = new HashMap<>(4);


    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    MyTimer() throws IOException {
        ConsoleHelper.getInstance();
        initMapOfEnums();
        initMapOfDeclension();
        setUnit();
        setDuration();
    }

    private int getDuration() {

        return duration;
    }

    private void setDuration() throws IOException {

            System.out.printf("Пожалуйста, введите время для занятий (%s): ", mapOfDeclension.get(unit));
            duration = ConsoleHelper.getInstance().getInt();
    }

    private void setUnit() throws IOException {

        boolean validTimeUnit = false;

        System.out.println("Добрый день. Этот простенький таймер подсчитает, когда вам можно остановиться в своих изысканиях.");

        while (!validTimeUnit) {

            System.out.println("Для начала, введите временной отрезок, в котором мы будем считать время (секунды/минуты/часы/дни): ");
            unit = ConsoleHelper.getInstance().getString();

            try {
                if (mapOfDeclension.containsKey(unit)) {validTimeUnit = true;}
                else System.out.println("Неверные данные");

            } catch (NumberFormatException e) {
                System.out.println("Неверные данные");
            }
        }
    }

    private void initMapOfDeclension() {
        mapOfDeclension.put("секунды", "секунд");
        mapOfDeclension.put("минуты", "минут");
        mapOfDeclension.put("часы", "часов");
        mapOfDeclension.put("дни", "дней");
    }

    private void initMapOfEnums() {
        mapOfEnums.put("секунды", timeUnit.SECONDS);
        mapOfEnums.put("минуты", timeUnit.MINUTES);
        mapOfEnums.put("часы", timeUnit.HOURS);
        mapOfEnums.put("дни", timeUnit.DAYS);
    }

    @Override
    public void run() {

        executorService.scheduleWithFixedDelay(this, 1, getDuration()+1, TimeUnit.valueOf(mapOfEnums.get(unit).toString()));

            if (getDuration() > 0) {
                System.out.printf("Осталось заниматься %d %s", getDuration(), NumAsString.getStringFromNum(unit, getDuration()));
                System.out.println();
                duration--;
            }

            if (duration == 0) {
                System.out.println("Занятия окончены! Поздравляем, вы молодцы");
                System.exit(0);
            }
    }
    /** этот класс поможет нам получить правильное склонение цифры в правильном временном формате (секунда/минута/час/день) **/
    private static final class NumAsString {

        private static final NumAsString sec = new NumAsString("секунды", "секунд", "секунда", "секунды");
        private static final NumAsString min = new NumAsString("минуты", "минут", "минуту", "минуты");
        private static final NumAsString hour = new NumAsString("часы", "часов", "час", "часа");
        private static final NumAsString day = new NumAsString("дни", "дней", "день", "дня");

        private String declensDefault;
        private String declens056789;
        private String declens1;
        private String declens234;

        private NumAsString(String sDefault, String s056789, String s1, String s234) {
            declensDefault = sDefault;
            declens056789 = s056789;
            declens1 = s1;
            declens234 = s234;
        }
        static String getStringFromNum(String aUnit, int aNum) {

            NumAsString result = new NumAsString("","","","");
            /** для склонения, берем последнюю цифру из пришедшего числа, например, из 179 получаем 9 **/
            int num = Integer.parseInt(Integer.toString(aNum).substring(String.valueOf(aNum).length()-1));

            switch (aUnit) {
                case "секунды" : {result = sec; break;}
                case "минуты" : {result = min; break;}
                case "часы" : {result = hour; break;}
                case "дни" : {result = day; break;}
                default : break;
            }

            switch (num) {
                case 0 : {return result.declens056789;}
                case 1 : {return result.declens1;}
                case 2 : {return result.declens234;}
                case 3 : {return result.declens234;}
                case 4 : {return result.declens234;}
                case 5 : {return result.declens056789;}
                case 6 : {return result.declens056789;}
                case 7 : {return result.declens056789;}
                case 8 : {return result.declens056789;}
                case 9 : {return result.declens056789;}
                default : return result.declensDefault;
            }
        }
    }
}
